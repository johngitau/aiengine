/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SMTPProtocol.h"
#include <iomanip> // setw

namespace aiengine {

#ifdef HAVE_LIBLOG4CXX
log4cxx::LoggerPtr SMTPProtocol::logger(log4cxx::Logger::getLogger("aiengine.smtp"));
#endif

// List of support commands 
std::vector<SmtpCommandType> SMTPProtocol::commands_ {
        std::make_tuple("EHLO"      	,4,     "hellos"     	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_EHLO)),
        std::make_tuple("AUTH LOGIN"  	,10,    "auth logins"  	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_AUTH)),
        std::make_tuple("MAIL FROM:"    ,10,    "mail froms"	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_MAIL)),
        std::make_tuple("RCPT TO:"      ,8,     "rcpt tos"      ,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_RCPT)),
        std::make_tuple("DATA"       	,4,     "datas"       	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_DATA)),
        std::make_tuple("EXPN"         	,4,     "expandss"     	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_EXPN)),
        std::make_tuple("VRFY"        	,4,     "verifys"       ,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_VRFY)),
        std::make_tuple("RSET"         	,4,     "resets"        ,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_RSET)),
        std::make_tuple("HELP"         	,4,     "helps"        	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_HELP)),
        std::make_tuple("NOOP"         	,4,     "noops"        	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_NOOP)),	
        std::make_tuple("QUIT"         	,4,     "quits"        	,0,	static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_QUIT))	
};

SMTPProtocol::SMTPProtocol():
	Protocol("SMTPProtocol","smtp"),
	smtp_header_(nullptr),
	total_events_(0),
	total_allow_domains_(0),
	total_ban_domains_(0),
	total_smtp_client_commands_(0),
	total_smtp_server_responses_(0),
	domain_mng_(),
	ban_domain_mng_(),
	info_cache_(new Cache<SMTPInfo>("SMTP Info cache")),
	from_cache_(new Cache<StringCache>("From cache")),
	to_cache_(new Cache<StringCache>("To cache")),
	from_map_(),
	to_map_(),
	flow_mng_(),
	current_flow_(nullptr),
	anomaly_(),
	cache_mng_() 
	{}

SMTPProtocol::~SMTPProtocol() { 

	anomaly_.reset(); 
	cache_mng_.reset(); 
}

bool SMTPProtocol::smtpChecker(Packet &packet) {

	// The first message comes from the server and have code 220
	if ((std::memcmp("220",packet.getPayload(),3) == 0) and
		((packet.getSourcePort() == 25) or
		(packet.getSourcePort() == 2525) or
		(packet.getSourcePort() == 587))) {

		++total_validated_packets_;
		return true;
	} else {
		++total_malformed_packets_;
		return false;
	}
}

void SMTPProtocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);	
	from_cache_->setDynamicAllocatedMemory(value);	
	to_cache_->setDynamicAllocatedMemory(value);	
}

bool SMTPProtocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();
}

int64_t SMTPProtocol::getCurrentUseMemory() const {

	int64_t mem = sizeof(SMTPProtocol);

	mem += info_cache_->getCurrentUseMemory();
	mem += from_cache_->getCurrentUseMemory();
	mem += to_cache_->getCurrentUseMemory();

	return mem;
}

int64_t SMTPProtocol::getAllocatedMemory() const {

	int64_t mem = sizeof(SMTPProtocol);

        mem += info_cache_->getAllocatedMemory();
        mem += from_cache_->getAllocatedMemory();
        mem += to_cache_->getAllocatedMemory();

        return mem;
}

int64_t SMTPProtocol::getTotalAllocatedMemory() const {

        int64_t mem = getAllocatedMemory();

	mem += compute_memory_used_by_maps();

	return mem;
}

// Removes or decrements the hits of the maps.
__attribute__ ((unused)) void SMTPProtocol::release_smtp_info_cache(SMTPInfo *info) {

        SharedPointer<StringCache> from_ptr = info->from;

        if (from_ptr) { // There is no from attached
                GenericMapType::iterator it = from_map_.find(from_ptr->getName());
                if (it != from_map_.end()) {
                        int *counter = &std::get<1>(it->second);
                        --(*counter);

                        if ((*counter) <= 0) {
                                from_map_.erase(it);
                        }
                }
        }

        SharedPointer<StringCache> to_ptr = info->to;

        if (to_ptr) { // There is a to attached 
                GenericMapType::iterator it = to_map_.find(to_ptr->getName());
                if (it != to_map_.end()) {
                        int *counter = &std::get<1>(it->second);
                        --(*counter);

                        if ((*counter) <= 0) {
                                to_map_.erase(it);
                        }
                }
        }

        release_smtp_info(info);
}

int32_t SMTPProtocol::release_smtp_info(SMTPInfo *info) {

        int32_t bytes_released = 0;

	bytes_released = releaseStringToCache(from_cache_,info->from);
	bytes_released += releaseStringToCache(to_cache_,info->to);

        info->resetStrings();

        return bytes_released;
}

int64_t SMTPProtocol::compute_memory_used_by_maps() const {

	int64_t bytes = 0;

	// Compute the size of the strings used as keys on the map
	std::for_each (from_map_.begin(), from_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});
	std::for_each (to_map_.begin(), to_map_.end(), [&bytes] (PairStringCacheHits const &t) {
		bytes += t.first.size();
	});
	return bytes;
}

int32_t SMTPProtocol::getTotalCacheMisses() const {

	int32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += from_cache_->getTotalFails();
	miss += to_cache_->getTotalFails();
	
	return miss;
}

void SMTPProtocol::releaseCache() {

	FlowManagerPtr fm = flow_mng_.lock();

	if (fm) {
		auto ft = fm->getFlowTable();

		std::ostringstream msg;
        	msg << "Releasing " << getName() << " cache";

		infoMessage(msg.str());

		int64_t total_bytes_released = compute_memory_used_by_maps();
		int64_t total_bytes_released_by_flows = 0;
		int32_t release_flows = 0;
		int32_t release_froms = from_map_.size();
		int32_t release_tos = to_map_.size();

                for (auto &flow: ft) {
                       	SharedPointer<SMTPInfo> info = flow->getSMTPInfo();
			if (info) {
                                total_bytes_released_by_flows += release_smtp_info(info.get());
                                total_bytes_released_by_flows += sizeof(info);
                               
                                flow->layer7info.reset();
                                ++ release_flows;
                                info_cache_->release(info);
                        }
                }
                from_map_.clear();
                to_map_.clear();

                double cache_compression_rate = 0;

                if (total_bytes_released_by_flows > 0 ) {
                        cache_compression_rate = 100 - ((total_bytes_released*100)/total_bytes_released_by_flows);
                }

                msg.str("");
                msg << "Release " << release_froms;
                msg << " froms, " << release_tos << " tos, " << release_flows << " flows";
                msg << ", " << total_bytes_released << " bytes, compression rate " << cache_compression_rate << "%";
                infoMessage(msg.str());
	}
}


void SMTPProtocol::attach_from(SMTPInfo *info, boost::string_ref &from) {

	if (!info->from) {
                GenericMapType::iterator it = from_map_.find(from);
                if (it == from_map_.end()) {
                        SharedPointer<StringCache> from_ptr = from_cache_->acquire();
                        if (from_ptr) {
                                from_ptr->setName(from.data(),from.length());
                                info->from = from_ptr;
                                from_map_.insert(std::make_pair(boost::string_ref(from_ptr->getName()),
					std::make_pair(from_ptr,1)));
                        }
                } else {
                        int *counter = &std::get<1>(it->second);
                        ++(*counter);
                        info->from = std::get<0>(it->second);
                }
        }
}

void SMTPProtocol::handle_cmd_mail(SMTPInfo *info, boost::string_ref &header) {

	SharedPointer<StringCache> from_ptr = info->from;

	size_t start = header.find_first_of("<");
	size_t end = header.find_first_of(">");

	if ((start > header.length())or(end > header.length())) {
		++total_events_;
                if (current_flow_->getPacketAnomaly() == PacketAnomalyType::NONE) {
                        current_flow_->setPacketAnomaly(PacketAnomalyType::SMTP_BOGUS_HEADER);
                }
		anomaly_->incAnomaly(current_flow_,PacketAnomalyType::SMTP_BOGUS_HEADER);
		return;
	}

	boost::string_ref from(header.substr(start + 1, end - start - 1));

	size_t token = from.find_first_of("@");

	if (token > from.length()) {
		++total_events_;
                if (current_flow_->getPacketAnomaly() == PacketAnomalyType::NONE) {
                        current_flow_->setPacketAnomaly(PacketAnomalyType::SMTP_BOGUS_HEADER);
                }
		anomaly_->incAnomaly(current_flow_,PacketAnomalyType::SMTP_BOGUS_HEADER);
		return;
	}
	boost::string_ref domain(from.substr(token + 1,from.size()));

	if (ban_domain_mng_) {
                auto dom_candidate = ban_domain_mng_->getDomainName(domain);
                if (dom_candidate) {
#ifdef HAVE_LIBLOG4CXX
                        LOG4CXX_INFO (logger, "Flow:" << *current_flow_ << " matchs with ban host " << dom_candidate->getName());
#endif
                        ++total_ban_domains_;
			info->setIsBanned(true);
                        return;
                }
        }
        ++total_allow_domains_;

	attach_from(info,from);

	if (domain_mng_) {
        	auto dom_candidate = domain_mng_->getDomainName(domain);
                if (dom_candidate) {
			++total_events_;
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
#ifdef HAVE_LIBLOG4CXX
			LOG4CXX_INFO (logger, "Flow:" << *current_flow_ << " matchs with " << dom_candidate->getName());
#endif
                        if (dom_candidate->call.haveCallback()) {
                       		dom_candidate->call.executeCallback(current_flow_);
                        }
#endif
                }
	}
}

void SMTPProtocol::handle_cmd_rcpt(SMTPInfo *info, boost::string_ref &header) {

	if (!info->to) {
        	size_t start = header.find_first_of("<");
        	size_t end = header.rfind(">");

		boost::string_ref to(header.substr(start + 1,end - start - 1));
                GenericMapType::iterator it = to_map_.find(to);
                if (it == to_map_.end()) {
                        SharedPointer<StringCache> to_ptr = to_cache_->acquire();
                        if (to_ptr) {
                                to_ptr->setName(to.data(),to.length());
                                info->to = to_ptr;
                                to_map_.insert(std::make_pair(boost::string_ref(to_ptr->getName()),
					std::make_pair(to_ptr,1)));
                        }
                } else {
                        int *counter = &std::get<1>(it->second);
                        ++(*counter);
                        info->to = std::get<0>(it->second);
                }
        }
}

void SMTPProtocol::processFlow(Flow *flow) {

	int length = flow->packet->getLength();
	unsigned char *payload = flow->packet->getPayload();
	total_bytes_ += length;
	++total_packets_;

	setHeader(payload);

       	SharedPointer<SMTPInfo> sinfo = flow->getSMTPInfo();

       	if (!sinfo) {
               	sinfo = info_cache_->acquire();
               	if (!sinfo) {
#ifdef HAVE_LIBLOG4CXX
			LOG4CXX_WARN (logger, "No memory on '" << info_cache_->getName() << "' for flow:" << *flow);
#endif
			return;
               	}
        	flow->layer7info = sinfo;
	}

        if (sinfo->getIsBanned() == true) {
		// No need to process the SMTP pdu.
                return;
        }

	current_flow_ = flow;

	if (flow->getFlowDirection() == FlowDirection::FORWARD) {
	
		if (sinfo->getIsData()) { // The client is transfering the email
			sinfo->incTotalDataBytes(length);

			// Check if is the last data block
			int offset = length - 7;
			if (offset > 0) {
				if (std::memcmp(&payload[offset],"\x0d\x0a\x0d\x0a\x2e\x0d\x0a",7) == 0) {
					sinfo->incTotalDataBlocks();
					sinfo->setIsData(false);
				}
			}	
		} else { // Commands send by the client
        		for (auto &command: commands_) {
                		const char *c = std::get<0>(command);
                		int offset = std::get<1>(command);

                		if (std::memcmp(c,&smtp_header_[0],offset) == 0) {
                        		int32_t *hits = &std::get<3>(command);
					int8_t cmd = std::get<4>(command);

                        		++(*hits);
					++total_smtp_client_commands_;

					// Check if the commands are MAIL or RCPT
					if ( cmd == static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_MAIL)) {
						boost::string_ref header(reinterpret_cast<const char*>(smtp_header_),length);
						handle_cmd_mail(sinfo.get(),header);
					} else if ( cmd == static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_RCPT)) {
						boost::string_ref header(reinterpret_cast<const char*>(smtp_header_),length);
						handle_cmd_rcpt(sinfo.get(),header);
					} else if ( cmd == static_cast<int8_t>(SMTPCommandTypes::SMTP_CMD_DATA)) {
						sinfo->setIsData(true);
					}	
					sinfo->setCommand(cmd);
                        		return;
                		}
        		}
		}
	} else {
		// Responses from the server

        	try {
			const char *header = reinterpret_cast<const char*>(smtp_header_);
			std::string value(header,3);

                	int code __attribute__((unused)) = std::stoi(value);
			
			++total_smtp_server_responses_;
        	} catch(std::invalid_argument&) { //or catch(...) to catch all exceptions
                	// We dont really do nothing here with code;
        	}
	}
	
	return;
} 

void SMTPProtocol::setDomainNameManager(const SharedPointer<DomainNameManager>& dnm) {

        if (domain_mng_) {
                domain_mng_->setPluggedToName("");
        }
	if (dnm) {
        	domain_mng_ = dnm;
        	domain_mng_->setPluggedToName(getName());
	} else {
		domain_mng_.reset();
	}
}

void SMTPProtocol::statistics(std::basic_ostream<char>& out, int level) {

	if (level > 0) {
                int64_t alloc_memory = getAllocatedMemory();
                std::string unit = "Bytes";
		const char *dynamic_memory = (isDynamicAllocatedMemory() ? "yes":"no");

                unitConverter(alloc_memory,unit);

                out << getName() << "(" << this <<") statistics" << std::dec << std::endl;

                if (ban_domain_mng_) out << "\t" << "Plugged banned domains from:" << ban_domain_mng_->getName() << std::endl;
                if (domain_mng_) out << "\t" << "Plugged domains from:" << domain_mng_->getName() << std::endl;

		out << "\t" << "Dynamic memory alloc:   " << std::setw(10) << dynamic_memory << std::endl;
                out << "\t" << "Total allocated:        " << std::setw(9 - unit.length()) << alloc_memory << " " << unit <<std::endl;
        	out << "\t" << "Total packets:          " << std::setw(10) << total_packets_ <<std::endl;
        	out << "\t" << "Total bytes:            " << std::setw(10) << total_bytes_ <<std::endl;
		if (level > 1) {
		
			out << "\t" << "Total validated packets:" << std::setw(10) << total_validated_packets_ <<std::endl;
			out << "\t" << "Total malformed packets:" << std::setw(10) << total_malformed_packets_ <<std::endl;
                        if (level > 3) {

                                out << "\t" << "Total allow domains:    " << std::setw(10) << total_allow_domains_ <<std::endl;
                                out << "\t" << "Total banned domains:   " << std::setw(10) << total_ban_domains_ <<std::endl;
                                out << "\t" << "Total client commands:  " << std::setw(10) << total_smtp_client_commands_ <<std::endl;
                                out << "\t" << "Total server responses: " << std::setw(10) << total_smtp_server_responses_ <<std::endl;

                                for (auto &command: commands_) {
                                        const char *label = std::get<2>(command);
                                        int32_t hits = std::get<3>(command);
                                        out << "\t" << "Total " << label << ":" << std::right << std::setfill(' ') << std::setw(27 - strlen(label)) << hits <<std::endl;

                                }
                        }
	
			if (level > 2) {	
			
				if (flow_forwarder_.lock())
					flow_forwarder_.lock()->statistics(out);
                                if (level > 3) {
                                        info_cache_->statistics(out);
                                        from_cache_->statistics(out);
                                        to_cache_->statistics(out);
                                        if (level > 4) {
                                                showCacheMap(out,from_map_,"SMTP Froms","From");
                                                showCacheMap(out,to_map_,"SMTP Tos","To");
                                        }
                                }
			}
		}
	}
}


void SMTPProtocol::increaseAllocatedMemory(int value) { 

	info_cache_->create(value);
	from_cache_->create(value);
	to_cache_->create(value);
}

void SMTPProtocol::decreaseAllocatedMemory(int value) { 

	info_cache_->destroy(value);
	from_cache_->destroy(value);
	to_cache_->destroy(value);
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict SMTPProtocol::getCounters() const {
        boost::python::dict counters;
#elif defined(RUBY_BINDING)
VALUE SMTPProtocol::getCounters() const {
        VALUE counters = rb_hash_new();
#elif defined(LUA_BINDING)
LuaCounters SMTPProtocol::getCounters() const {
	LuaCounters counters;
#endif
        addValueToCounter(counters,"packets", total_packets_);
        addValueToCounter(counters,"bytes", total_bytes_);
        addValueToCounter(counters,"commands", total_smtp_client_commands_);
        addValueToCounter(counters,"responses", total_smtp_server_responses_);

        for (auto &command: commands_) {
                const char *label = std::get<2>(command);

                addValueToCounter(counters,label,std::get<3>(command));
        }
        return counters;
}

#endif

} // namespace aiengine

