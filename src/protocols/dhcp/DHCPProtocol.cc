/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DHCPProtocol.h"
#include <iomanip>

namespace aiengine {

#ifdef HAVE_LIBLOG4CXX
log4cxx::LoggerPtr DHCPProtocol::logger(log4cxx::Logger::getLogger("aiengine.dhcp"));
#endif

DHCPProtocol::DHCPProtocol():
	Protocol("DHCPProtocol","dhcp"),
        dhcp_header_(nullptr),
        total_dhcp_discover_(0),
        total_dhcp_offer_(0),
        total_dhcp_request_(0),
        total_dhcp_decline_(0),
        total_dhcp_ack_(0),
        total_dhcp_nak_(0),
        total_dhcp_release_(0),
        total_dhcp_inform_(0), 
        info_cache_(new Cache<DHCPInfo>("DHCP Info cache")),
        host_cache_(new Cache<StringCache>("Host cache")),
        host_map_(),
	flow_mng_(),
        current_flow_(nullptr),
        anomaly_(),
        cache_mng_() 
	{} 

bool DHCPProtocol::dhcpChecker(Packet &packet) {

	int length = packet.getLength();

	if (length >= header_size) {
		if ((packet.getSourcePort() == 67)||(packet.getDestinationPort() == 67)) {
			setHeader(packet.getPayload());
			++total_validated_packets_;
			return true;
		}
	}
	++total_malformed_packets_;
	return false;
}


void DHCPProtocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);
	host_cache_->setDynamicAllocatedMemory(value);
}

bool DHCPProtocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();
}

int32_t DHCPProtocol::release_dhcp_info(DHCPInfo *info) {

        int32_t bytes_released = 0;

	bytes_released = releaseStringToCache(host_cache_,info->host_name);

        return bytes_released;
}

int64_t DHCPProtocol::getCurrentUseMemory() const {

	int64_t mem = sizeof(DHCPProtocol);

	mem += info_cache_->getCurrentUseMemory();
	mem += host_cache_->getCurrentUseMemory();
	
	return mem;
}

int64_t DHCPProtocol::getAllocatedMemory() const {

        int64_t mem = sizeof(DHCPProtocol);

        mem += info_cache_->getAllocatedMemory();
        mem += host_cache_->getAllocatedMemory();

        return mem;
}

int64_t DHCPProtocol::getTotalAllocatedMemory() const {

        return getAllocatedMemory();
}

int64_t DHCPProtocol::compute_memory_used_by_maps() const {

	int64_t bytes = 0;

	std::for_each (host_map_.begin(), host_map_.end(), [&bytes] (PairStringCacheHits const &f) {
		bytes += f.first.size();
	});

	return bytes;
}

int32_t DHCPProtocol::getTotalCacheMisses() const {

	int32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += host_cache_->getTotalFails();

	return miss;
}

void DHCPProtocol::releaseCache() {

        FlowManagerPtr fm = flow_mng_.lock();

        if (fm) {
                auto ft = fm->getFlowTable();

                std::ostringstream msg;
                msg << "Releasing " << getName() << " cache";

                infoMessage(msg.str());

                int64_t total_bytes_released = compute_memory_used_by_maps();
                int64_t total_bytes_released_by_flows = 0;
                int32_t release_flows = 0;
                int32_t release_host = host_map_.size();

                for (auto &flow: ft) {
                        SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
                        if (info) {
                                total_bytes_released_by_flows = release_dhcp_info(info.get());
                                total_bytes_released_by_flows += sizeof(info);

                                flow->layer7info.reset();
                                ++ release_flows;
                                info_cache_->release(info);
                        }
                }
                host_map_.clear();

                double cache_compression_rate = 0;

                if (total_bytes_released_by_flows > 0 ) {
                        cache_compression_rate = 100 - ((total_bytes_released*100)/total_bytes_released_by_flows);
                }

                msg.str("");
                msg << "Release " << release_host << " host names, " << release_flows << " flows";
                msg << ", " << total_bytes_released << " bytes, compression rate " << cache_compression_rate << "%";
                infoMessage(msg.str());
        }
}

void DHCPProtocol::attach_host_name(DHCPInfo *info, boost::string_ref &name) {

        if (!info->host_name) {
                GenericMapType::iterator it = host_map_.find(name);
                if (it == host_map_.end()) {
                        SharedPointer<StringCache> host_ptr = host_cache_->acquire();
                        if (host_ptr) {
                                host_ptr->setName(name.data(),name.length());
                                info->host_name = host_ptr;
                                host_map_.insert(std::make_pair(boost::string_ref(host_ptr->getName()),
                                        std::make_pair(host_ptr,1)));
                        }
                } else {
                        int *counter = &std::get<1>(it->second);
                        ++(*counter);
                        info->host_name = std::get<0>(it->second);
                }
        }
}

void DHCPProtocol::handle_request(DHCPInfo *info, unsigned char *payload, int length) {

        int idx = 0;
        while (idx < length) {
        	short type = payload[idx];
                short len = payload[idx+1];

                if (type == 12) { // Hostname
			boost::string_ref name(reinterpret_cast<const char*>(&payload[idx+2]),len);

                        attach_host_name(info,name);
                        break;
		}
                idx += 2 + (int)len;
	}
}

void DHCPProtocol::handle_reply(DHCPInfo *info, unsigned char *payload, int length) {

        int idx = 0;
        while (idx < length) {
                short type = payload[idx];
                short len = payload[idx+1];

                if (type == 51) { // IP Lease time
			int32_t lease_time = (payload[idx+2] << 24) | (payload[idx+3] << 16) | (payload[idx+4] << 8) | payload[idx+5];

			info->setLeaseTime(lease_time);
                        break;
                }
                idx += 2 + (int)len;
        }
}


void DHCPProtocol::processFlow(Flow *flow) {

	setHeader(flow->packet->getPayload());	
	uint8_t msgtype = getType();
	int length = flow->packet->getLength();
	total_bytes_ += length;

	current_flow_ = flow;

	++total_packets_;

	// if there is no magic, then there is no request
	if ((length > header_size)and(std::memcmp(&dhcp_header_->magic,"\x63\x82\x53\x63",4) == 0)) { 
		// TODO: Retrieve the IP address and mac for detect roque dhcp servers

                SharedPointer<DHCPInfo> dinfo = flow->getDHCPInfo();
                if (!dinfo) {
                        dinfo = info_cache_->acquire();
                        if (!dinfo) {
#ifdef HAVE_LIBLOG4CXX
				LOG4CXX_WARN (logger, "No memory on '" << info_cache_->getName() << "' for flow:" << *flow);
#endif
				return;
                        }
                        flow->layer7info = dinfo;
                }

		int options_length = length - header_size;
		unsigned char *optpayload = &dhcp_header_->opt[0];

		short otype = optpayload[0];
		if (otype == 53) { // Extract the dhcp message type
			short type = optpayload[2];

			if (type == DHCPDISCOVER) {
				++total_dhcp_discover_;
			} else if (type == DHCPOFFER) {
				++total_dhcp_offer_;
			} else if (type == DHCPREQUEST) {
				++total_dhcp_request_;
			} else if (type == DHCPDECLINE) {
				++total_dhcp_decline_;
			} else if (type == DHCPACK) {
				++total_dhcp_ack_;
			} else if (type == DHCPNAK) {
				++total_dhcp_nak_;
			} else if (type == DHCPRELEASE) {
				++total_dhcp_release_;
			} else if (type == DHCPINFORM) {
				++total_dhcp_inform_;
                	}
		}	

		if (msgtype == DHCP_BOOT_REQUEST) {
			handle_request(dinfo.get(),optpayload,options_length);
		} else {
			handle_reply(dinfo.get(),optpayload,options_length);
		}
	} else {
		// Malformed DHCP packet
                if (flow->getPacketAnomaly() == PacketAnomalyType::NONE) {
                	flow->setPacketAnomaly(PacketAnomalyType::DHCP_BOGUS_HEADER);
                }
                anomaly_->incAnomaly(PacketAnomalyType::DHCP_BOGUS_HEADER);
	}
}

void DHCPProtocol::statistics(std::basic_ostream<char>& out, int level){ 

	if (level > 0) {
                int64_t alloc_memory = getAllocatedMemory();
                std::string unit = "Bytes";
		const char *dynamic_memory = (isDynamicAllocatedMemory() ? "yes":"no");

                unitConverter(alloc_memory,unit);

                out << getName() << "(" << this <<") statistics" << std::dec << std::endl;
		out << "\t" << "Dynamic memory alloc:   " << std::setw(10) << dynamic_memory << std::endl;
		out << "\t" << "Total allocated:        " << std::setw(9 - unit.length()) << alloc_memory << " " << unit <<std::endl;
		out << "\t" << "Total packets:          " << std::setw(10) << total_packets_ <<std::endl;
		out << "\t" << "Total bytes:        " << std::setw(14) << total_bytes_ <<std::endl;
		if (level > 1) {
			out << "\t" << "Total validated packets:" << std::setw(10) << total_validated_packets_ <<std::endl;
			out << "\t" << "Total malformed packets:" << std::setw(10) << total_malformed_packets_ <<std::endl;
                        if (level > 3) {

                                out << "\t" << "Total discovers:        " << std::setw(10) << total_dhcp_discover_ <<std::endl;
                                out << "\t" << "Total offers:           " << std::setw(10) << total_dhcp_offer_ <<std::endl;
                                out << "\t" << "Total requests:         " << std::setw(10) << total_dhcp_request_ <<std::endl;
                                out << "\t" << "Total declines:         " << std::setw(10) << total_dhcp_decline_ <<std::endl;
                                out << "\t" << "Total acks:             " << std::setw(10) << total_dhcp_ack_ <<std::endl;
                                out << "\t" << "Total naks:             " << std::setw(10) << total_dhcp_nak_ <<std::endl;
                                out << "\t" << "Total releases:         " << std::setw(10) << total_dhcp_release_ <<std::endl;
                                out << "\t" << "Total informs:          " << std::setw(10) << total_dhcp_inform_ <<std::endl;
                        }
			if (level > 2) {
				if (mux_.lock())
					mux_.lock()->statistics(out);
                                if (flow_forwarder_.lock())
                                        flow_forwarder_.lock()->statistics(out);
                                if (level > 3) {
                                        info_cache_->statistics(out);
                                        host_cache_->statistics(out);
                                        if (level > 4) {
                                                showCacheMap(out,host_map_,"Host names","Host");
                                        }
                                }
			}
		}
	}
}

void DHCPProtocol::increaseAllocatedMemory(int value) {

        info_cache_->create(value);
        host_cache_->create(value);
}

void DHCPProtocol::decreaseAllocatedMemory(int value) {

        info_cache_->destroy(value);
        host_cache_->destroy(value);
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) 
#if defined(PYTHON_BINDING)
boost::python::dict DHCPProtocol::getCache() const {
#elif defined(RUBY_BINDING)
VALUE DHCPProtocol::getCache() const {
#endif
        return addMapToHash(host_map_);
}

#endif

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict DHCPProtocol::getCounters() const {
        boost::python::dict counters;
#elif defined(RUBY_BINDING)
VALUE DHCPProtocol::getCounters() const {
        VALUE counters = rb_hash_new();
#elif defined(LUA_BINDING)
LuaCounters DHCPProtocol::getCounters() const {
	LuaCounters counters;
#endif
        addValueToCounter(counters,"packets",total_packets_);
        addValueToCounter(counters,"bytes", total_bytes_);
        addValueToCounter(counters,"discovers", total_dhcp_discover_);
        addValueToCounter(counters,"offers", total_dhcp_offer_);
        addValueToCounter(counters,"requests", total_dhcp_request_);
        addValueToCounter(counters,"declines", total_dhcp_decline_);
        addValueToCounter(counters,"acks", total_dhcp_ack_);
        addValueToCounter(counters,"naks", total_dhcp_nak_);
        addValueToCounter(counters,"releases", total_dhcp_release_);
        addValueToCounter(counters,"informs", total_dhcp_inform_);

        return counters;
}

#endif

} // namespace aiengine
