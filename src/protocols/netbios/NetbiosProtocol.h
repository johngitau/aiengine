/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_
#define SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif

#include "Protocol.h"
#include "NetbiosInfo.h"
#include "CacheManager.h"
#include <arpa/inet.h>
#include "flow/FlowManager.h"

namespace aiengine {

typedef struct {
	uint16_t id;
	uint16_t flags;
	uint16_t questions;
	uint16_t answers;
	uint16_t auths;
	uint16_t adds;
	u_char data[0];
} netbios_hdr;

class NetbiosProtocol: public Protocol {
public:
    	explicit NetbiosProtocol();
    	virtual ~NetbiosProtocol();

	static const uint16_t id = 0;	
	static constexpr int header_size = sizeof(netbios_hdr);

	int getHeaderSize() const { return header_size;}

        void processFlow(Flow *flow);
        bool processPacket(Packet& packet) { return true; } 

	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char>& out, int level);
	void statistics() { statistics(std::cout);}

	void releaseCache(); 

	void setHeader(unsigned char *raw_packet){ 

		netbios_header_ = reinterpret_cast <netbios_hdr*> (raw_packet);
	}

	// Condition for say that a packet is netbios
	bool netbiosChecker(Packet &packet);

        void increaseAllocatedMemory(int value); 
        void decreaseAllocatedMemory(int value); 

        void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
        int64_t getAllocatedMemory() const; 
        int64_t getTotalAllocatedMemory() const; 

        void setDynamicAllocatedMemory(bool value);
        bool isDynamicAllocatedMemory() const;

	int32_t getTotalCacheMisses() const;
	int32_t getTotalEvents() const { return total_events_; }

#if defined(PYTHON_BINDING)
        boost::python::dict getCounters() const;
        boost::python::dict getCache() const;
#elif defined(RUBY_BINDING)
        VALUE getCounters() const;
        VALUE getCache() const;
#elif defined(JAVA_BINDING)
        JavaCounters getCounters() const;
#elif defined(LUA_BINDING)
        LuaCounters getCounters() const;
#endif
        void setAnomalyManager(SharedPointer<AnomalyManager> amng) { anomaly_ = amng; }
        void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(info_cache_); }

	Flow* getCurrentFlow() const { return current_flow_; }

private:

	void attach_netbios_name(NetbiosInfo *info, boost::string_ref &name);
	int32_t release_netbios_info(NetbiosInfo *info);
	int64_t compute_memory_used_by_maps() const;

	netbios_hdr *netbios_header_;
	int32_t total_events_;

        Cache<NetbiosInfo>::CachePtr info_cache_;
        Cache<StringCache>::CachePtr name_cache_;

        GenericMapType name_map_;

	FlowManagerPtrWeak flow_mng_;
	Flow *current_flow_;
#ifdef HAVE_LIBLOG4CXX
        static log4cxx::LoggerPtr logger;
#endif
        SharedPointer<AnomalyManager> anomaly_;
        SharedPointer<CacheManager> cache_mng_;
	unsigned char netbios_name_[32];
};

typedef std::shared_ptr<NetbiosProtocol> NetbiosProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_NETBIOS_NETBIOSPROTOCOL_H_
