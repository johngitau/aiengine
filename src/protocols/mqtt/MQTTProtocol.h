/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_MQTT_MQTTPROTOCOL_H_ 
#define SRC_PROTOCOLS_MQTT_MQTTPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif
#include "Protocol.h"
#include "MQTTInfo.h"
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include "CacheManager.h"
#include <unordered_map>
#include "names/DomainNameManager.h"
#include "flow/FlowManager.h"

namespace aiengine {

// Minimum MQTT header, for data and signaling
typedef struct {
	uint8_t type;
	uint8_t length;
	u_char data[0];
} __attribute__((packed)) mqtt_hdr;

typedef struct {
	uint8_t pad1;
	uint8_t pad2;
	char proto_name[4];
	uint8_t proto_level;
	uint8_t flags;
	uint16_t keep_alive;
} __attribute__((packed)) mqtt_connect_hdr;

enum class MQTTControlPacketTypes : std::int8_t {
	MQTT_CPT_RESERVED1 = 	0,
	MQTT_CPT_CONNECT ,  	
	MQTT_CPT_CONNACK ,  	
	MQTT_CPT_PUBLISH ,  	
	MQTT_CPT_PUBACK ,  	
	MQTT_CPT_PUBREC ,  	
	MQTT_CPT_PUBREL ,  	
	MQTT_CPT_PUBCOMP ,  	
	MQTT_CPT_SUBSCRIBE ,  	
	MQTT_CPT_SUBACK ,  	
	MQTT_CPT_UNSUBSCRIBE ,  	
	MQTT_CPT_UNSUBACK ,  	
	MQTT_CPT_PINGREQ ,  	
	MQTT_CPT_PINGRESP ,  	
	MQTT_CPT_DISCONNECT ,  	
	MQTT_CPT_RESERVED2  	
};

// Commands with statistics
typedef std::tuple<std::int8_t, const char*,int32_t> MqttControlPacketType;

class MQTTProtocol: public Protocol {
public:
    	explicit MQTTProtocol();
    	virtual ~MQTTProtocol(); 

	static const uint16_t id = 0;
	static const int header_size = sizeof(mqtt_hdr); 

	int getHeaderSize() const { return header_size;}

	bool processPacket(Packet& packet) { return true; }
	void processFlow(Flow *flow);

	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char>& out, int level);
	void statistics() { statistics(std::cout); }

	void releaseCache(); 

        void setHeader(unsigned char *raw_packet) {
                
		mqtt_header_ = reinterpret_cast<mqtt_hdr*>(raw_packet);
        }

	// Condition for say that a payload is MQTT 
	bool mqttChecker(Packet &packet); 

	int8_t getCommandType() const { return mqtt_header_->type >> 4; }
	uint8_t getFlags() const { return mqtt_header_->type & 0x0F; }
	int32_t getLength(); 

	int32_t getTotalClientCommands() const { return total_mqtt_client_commands_; }
	int32_t getTotalServerCommands() const { return total_mqtt_server_responses_; }

        void increaseAllocatedMemory(int value); 
        void decreaseAllocatedMemory(int value); 

	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
	int64_t getAllocatedMemory() const;
	int64_t getTotalAllocatedMemory() const;

        void setDynamicAllocatedMemory(bool value); 
        bool isDynamicAllocatedMemory() const; 

	int32_t getTotalCacheMisses() const;
	int32_t getTotalEvents() const { return total_events_; }

	Flow *getCurrentFlow() const { return current_flow_; }

#if defined(PYTHON_BINDING)
        boost::python::dict getCounters() const;
        boost::python::dict getCache() const;
#elif defined(RUBY_BINDING)
        VALUE getCounters() const;
        VALUE getCache() const;
#elif defined(JAVA_BINDING)
        JavaCounters getCounters() const  { JavaCounters counters; return counters; }
#elif defined(LUA_BINDING)
        LuaCounters getCounters() const;
#endif
	void setAnomalyManager(SharedPointer<AnomalyManager> amng) { anomaly_ = amng; }
	void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(info_cache_); }
private:
	void release_mqtt_info_cache(MQTTInfo *info);
	int32_t release_mqtt_info(MQTTInfo *info);
	int64_t compute_memory_used_by_maps() const;

	void attach_topic(MQTTInfo *info, boost::string_ref &topic);
	void handle_publish_message(MQTTInfo *info, unsigned char *payload, int length);

	mqtt_hdr *mqtt_header_;
        int32_t total_events_;

	static std::vector<MqttControlPacketType> commands_;
	
	int32_t total_mqtt_client_commands_;
	int32_t total_mqtt_server_responses_;

	int8_t length_offset_;

        Cache<MQTTInfo>::CachePtr info_cache_;
        Cache<StringCache>::CachePtr topic_cache_;

	GenericMapType topic_map_;

	FlowManagerPtrWeak flow_mng_;
	Flow *current_flow_;	
#ifdef HAVE_LIBLOG4CXX
	static log4cxx::LoggerPtr logger;
#endif
	SharedPointer<AnomalyManager> anomaly_;
	SharedPointer<CacheManager> cache_mng_;
};

typedef std::shared_ptr<MQTTProtocol> MQTTProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_MQTT_MQTTPROTOCOL_H_
