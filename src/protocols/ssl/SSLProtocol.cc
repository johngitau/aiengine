/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "SSLProtocol.h"
#include <iomanip> // setw

namespace aiengine {

#ifdef HAVE_LIBLOG4CXX
log4cxx::LoggerPtr SSLProtocol::logger(log4cxx::Logger::getLogger("aiengine.ssl"));
#endif

SSLProtocol::SSLProtocol():
	Protocol("SSLProtocol","ssl"),
	ssl_header_(nullptr),
	total_events_(0),
	total_client_hellos_(0),
	total_server_hellos_(0),
	total_certificates_(0),
        total_server_key_exchanges_(0),
        total_certificate_requests_(0),
	total_server_dones_(0),
	total_certificate_verifies_(0),
	total_client_key_exchanges_(0),
	total_handshake_finishes_(0),
	total_records_(0),
	total_ban_hosts_(0),
	total_allow_hosts_(0),
	info_cache_(new Cache<SSLInfo>("SSL Info cache")),
	host_cache_(new Cache<StringCache>("Host cache")),
	host_map_(),
	domain_mng_(),
	ban_domain_mng_(),
	flow_mng_(),
	current_flow_(nullptr),
	anomaly_(),
	cache_mng_() 
	{}

SSLProtocol::~SSLProtocol() { 

	anomaly_.reset(); 
	cache_mng_.reset(); 
}

bool SSLProtocol::sslChecker(Packet &packet) {

	if (std::memcmp("\x16\x03",packet.getPayload(),2)==0) {
		setHeader(packet.getPayload());
		++total_validated_packets_;
		return true;
	} else {
		++total_malformed_packets_;
		return false;
	}
}

void SSLProtocol::setDynamicAllocatedMemory(bool value) {

	info_cache_->setDynamicAllocatedMemory(value);
	host_cache_->setDynamicAllocatedMemory(value);
}

bool SSLProtocol::isDynamicAllocatedMemory() const {

	return info_cache_->isDynamicAllocatedMemory();	
}

int64_t SSLProtocol::getCurrentUseMemory() const {

	int64_t mem = sizeof(SSLProtocol);

	mem += info_cache_->getCurrentUseMemory();
	mem += host_cache_->getCurrentUseMemory();

	return mem;
}

int64_t SSLProtocol::getAllocatedMemory() const {

        int64_t mem = sizeof(SSLProtocol); 

        mem += info_cache_->getAllocatedMemory();
        mem += host_cache_->getAllocatedMemory();

        return mem;
}

int64_t SSLProtocol::getTotalAllocatedMemory() const {

	int64_t mem = getAllocatedMemory();

	mem += compute_memory_used_by_maps();

	return mem;
}

int32_t SSLProtocol::release_ssl_info(SSLInfo *info) {

        int32_t bytes_released = 0;

	bytes_released = releaseStringToCache(host_cache_,info->host_name);

        return bytes_released;
}

// Removes or decrements the hits of the maps.
void SSLProtocol::release_ssl_info_cache(SSLInfo *info) {

        auto host_ptr = info->host_name;

        if (host_ptr) { // There is no host attached
                auto it = host_map_.find(host_ptr->getName());
                if (it != host_map_.end()) {
                        int *counter = &std::get<1>(it->second);
                        --(*counter);

                        if ((*counter) <= 0) {
                                host_map_.erase(it);
                        }
                }
        }

        release_ssl_info(info);
}

int64_t SSLProtocol::compute_memory_used_by_maps() const {

	int64_t bytes = 0;

        // Compute the size of the strings used as keys on the map
        std::for_each (host_map_.begin(), host_map_.end(), [&bytes] (PairStringCacheHits const &ht) {
        	bytes += ht.first.size();
	});
	
	return bytes;
}

int32_t SSLProtocol::getTotalCacheMisses() const {
	
	int32_t miss = 0;

	miss = info_cache_->getTotalFails();
	miss += host_cache_->getTotalFails();

	return miss;
}

void SSLProtocol::releaseCache() {

        FlowManagerPtr fm = flow_mng_.lock();

        if (fm) {
                auto ft = fm->getFlowTable();

                std::ostringstream msg;
                msg << "Releasing " << getName() << " cache";

                infoMessage(msg.str());

		int64_t total_bytes_released = compute_memory_used_by_maps();
		int64_t total_bytes_released_by_flows = 0;
                int32_t release_flows = 0;
                int32_t release_hosts = host_map_.size();

                for (auto &flow: ft) {
		       	auto sinfo = flow->getSSLInfo();
               		if (sinfo) { 
                                total_bytes_released_by_flows += release_ssl_info(sinfo.get());
                                total_bytes_released_by_flows += sizeof(sinfo);

                                flow->layer7info.reset();
                                info_cache_->release(sinfo);
				++release_flows;
                        }
                } 
	        host_map_.clear();

                double cache_compression_rate = 0;

                if (total_bytes_released_by_flows > 0 ) {
                        cache_compression_rate = 100 - ((total_bytes_released*100)/total_bytes_released_by_flows);
                }
        
        	msg.str("");
                msg << "Release " << release_hosts << " hosts, " << release_flows << " flows";
		msg << ", " << total_bytes_released << " bytes, compression rate " << cache_compression_rate << "%";
                infoMessage(msg.str());
        }
}

void SSLProtocol::attach_host(SSLInfo *info, boost::string_ref &host) {

	if (!info->host_name) {
                auto it = host_map_.find(host);
                if (it == host_map_.end()) {
                        auto host_ptr = host_cache_->acquire();
                        if (host_ptr) {
                                host_ptr->setName(host.data(),host.length());
                                info->host_name = host_ptr;
                                host_map_.insert(std::make_pair(boost::string_ref(host_ptr->getName()),
                                        std::make_pair(host_ptr,1)));
                        }
                } else {
                        int *counter = &std::get<1>(it->second);
                        ++(*counter);
                        info->host_name = std::get<0>(it->second);
                }
        }
}

void SSLProtocol::handle_client_hello(SSLInfo *info,int length,int offset, u_char *data) {

	ssl_hello *hello = reinterpret_cast<ssl_hello*>(data); 
	uint16_t version = ntohs(hello->version);
	int block_offset = sizeof(ssl_hello) + offset;

	++ total_client_hellos_;

	if ((version >= SSL3_VERSION)and(version <= TLS1_2_VERSION)) {

		if (ntohs(hello->length) > length) {
			++total_events_;
			if (current_flow_->getPacketAnomaly() == PacketAnomalyType::NONE) {
                        	current_flow_->setPacketAnomaly(PacketAnomalyType::SSL_BOGUS_HEADER);
			}
                        anomaly_->incAnomaly(current_flow_,PacketAnomalyType::SSL_BOGUS_HEADER);
                        return; 
		}
		length = ntohs(hello->length);

		if (ntohs(hello->session_id_length) > 0) {
			// Session id management
			// the alignment of the struct should be fix
			block_offset += 32;
		}

		uint16_t cipher_length = ntohs((data[block_offset+1] << 8) + data[block_offset]);
		if (cipher_length < length) {

			block_offset += cipher_length  + 2;
			u_char *compression_pointer = &data[block_offset];
			short compression_length = compression_pointer[0];
		
			if (compression_length > 0) {
				block_offset += (compression_length + 1);
			}
			if (block_offset < length) {
				u_char *extensions = &data[block_offset];
				uint16_t extensions_length = ((extensions[0] << 8) + extensions[1]);

				block_offset += 2;
				while (block_offset < length) {
					ssl_extension *extension = reinterpret_cast<ssl_extension*>(&data[block_offset]);
					if (extension->type == 0x0000) { // Server name
						ssl_server_name *server = reinterpret_cast<ssl_server_name*>(&extension->data[0]);
						int server_length = ntohs(server->length);
						
						if ((block_offset + server_length < length )and(server_length > 0)) {
							boost::string_ref servername((char*)server->data,server_length);
					
							if (ban_domain_mng_) {		
								auto host_candidate = ban_domain_mng_->getDomainName(servername);
								if (host_candidate) {
#ifdef HAVE_LIBLOG4CXX
									LOG4CXX_INFO (logger, "Flow:" << *current_flow_ << " matchs with banned host " << host_candidate->getName());
#endif
									++total_ban_hosts_;
									info->setIsBanned(true);
									return;
								}
							}
							++total_allow_hosts_;

							attach_host(info,servername);
						}	
					} else {
						if (extension->type == 0x01ff) { // Renegotiation
							// TODO std::cout << "RENEOGOTIATIONT" << std::endl;
						} else {
							if (extension->type == 0x0f00) { // Heartbeat
								info->setHeartbeat(true);
							} else {
								if (extension->type == 0x000d) {
									// TODO std::cout << "signature algorithm" << std::endl;
								}
							}
						}
					}	
					block_offset += ntohs(extension->length) + sizeof(ssl_extension) ;
				}
			}	
		}
	} // end version 
	return;
}

void SSLProtocol::handle_server_hello(SSLInfo *info,int offset,unsigned char *data) {

	ssl_hello *hello __attribute__((unused)) = reinterpret_cast<ssl_hello*>(data); 
	++ total_server_hellos_;
}

void SSLProtocol::handle_certificate(SSLInfo *info,int offset, unsigned char *data) {

	++ total_certificates_;
}

void SSLProtocol::processFlow(Flow *flow) {

	++total_packets_;
	total_bytes_ += flow->packet->getLength();
	++flow->total_packets_l7;

        auto sinfo = flow->getSSLInfo();
        if (!sinfo) {
                sinfo = info_cache_->acquire();
                if (!sinfo) {
#ifdef HAVE_LIBLOG4CXX
			LOG4CXX_WARN (logger, "No memory on '" << info_cache_->getName() << "' for flow:" << *flow);
#endif
			return;
                }
                flow->layer7info = sinfo;
        }

        if (sinfo->getIsBanned() == true) {
                // No need to process the SSL pdu.
                return;
        }

	current_flow_ = flow;

	setHeader(flow->packet->getPayload());
	if (flow->total_packets_l7 < 3) { 

		int length = ntohs(ssl_header_->length);
		if (length > 0) {
			ssl_record *record = ssl_header_;
			int offset = 0;		// Total offset byte
			int maxattemps = 0; 	// For prevent invalid decodings

			do {
				uint16_t version = ntohs(record->version);
				int block_length = ntohs(record->length);
				short type = record->data[0];
				++maxattemps;

				if ((version >= SSL3_VERSION)and(version <= TLS1_2_VERSION)) {

					sinfo->setVersion(version);

					// This is a valid SSL header that we could extract some usefulll information.
					// SSL Records are group by blocks

					u_char *ssl_data = record->data;
					bool have_data = false;
	
					if (type == SSL3_MT_CLIENT_HELLO)  {
						handle_client_hello(sinfo.get(),flow->packet->getLength(),offset,ssl_data);
						have_data = true;
					} else if (type == SSL3_MT_SERVER_HELLO)  {
						handle_server_hello(sinfo.get(),offset,ssl_data);
						have_data = true;
					} else if (type == SSL3_MT_CERTIFICATE) {
						handle_certificate(sinfo.get(),offset,ssl_data);
						have_data = true;
					} else if (type == SSL3_MT_SERVER_KEY_EXCHANGE) {
						++total_server_key_exchanges_;				
					} else if (type == SSL3_MT_CERTIFICATE_REQUEST) {
						++total_certificate_requests_;
					} else if (type == SSL3_MT_SERVER_DONE) {
						++total_server_dones_;
					} else if (type == SSL3_MT_CERTIFICATE_VERIFY) {
						++total_certificate_verifies_;
					} else if (type == SSL3_MT_CLIENT_KEY_EXCHANGE) {
						++total_client_key_exchanges_;
					} else if (type == SSL3_MT_FINISHED) {
						++total_handshake_finishes_;
					}

					if (have_data) {
						++ total_records_;
						offset += block_length;
						ssl_data = &(record->data[block_length]);
						block_length = ntohs(record->length);
					}

					if (offset + (2 * (int)sizeof(ssl_record)) > flow->packet->getLength()) {
						// The record is split in two packets
						break;
					}
					record = reinterpret_cast<ssl_record*>(ssl_data);
					offset += sizeof(ssl_record);	
				} else {
					break;
				}
				if (maxattemps == 4 ) break;
			} while (offset < flow->packet->getLength());

			if (flow->total_packets_l7 == 1) {
				if ((domain_mng_)and(sinfo->host_name)) { 
					auto host_candidate = domain_mng_->getDomainName(sinfo->host_name->getName());
					if (host_candidate) {
						++total_events_;
						sinfo->matched_domain_name = host_candidate;
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
#ifdef HAVE_LIBLOG4CXX
						LOG4CXX_INFO (logger, "Flow:" << *flow << " matchs with " << host_candidate->getName());
#endif  
						if (host_candidate->call.haveCallback()) {
							host_candidate->call.executeCallback(flow);
						}
#endif
					}
				}
			}
		}
	} else {
		// Check if the PDU is encrypted data
		if (std::memcmp("\x17\x03",ssl_header_,2)==0) {
			sinfo->incDataPdus();
		}
	}
}

void SSLProtocol::setDomainNameManager(const SharedPointer<DomainNameManager>& dnm) {

	if (domain_mng_) {
		domain_mng_->setPluggedToName("");
	}
	if (dnm) {
		domain_mng_ = dnm;
        	domain_mng_->setPluggedToName(getName());
	} else {
		domain_mng_.reset();
	}
}

void SSLProtocol::statistics(std::basic_ostream<char>& out, int level) {

	if (level > 0) {
                int64_t alloc_memory = getAllocatedMemory();
                std::string unit = "Bytes";
		const char *dynamic_memory = (isDynamicAllocatedMemory() ? "yes":"no");

                unitConverter(alloc_memory,unit);

                out << getName() << "(" << this <<") statistics" << std::dec << std::endl;

                if (ban_domain_mng_) out << "\t" << "Plugged banned domains from:" << ban_domain_mng_->getName() << std::endl;
                if (domain_mng_) out << "\t" << "Plugged domains from:" << domain_mng_->getName() << std::endl;

		out << "\t" << "Dynamic memory alloc:   " << std::setw(10) << dynamic_memory << std::endl;
                out << "\t" << "Total allocated:        " << std::setw(9 - unit.length()) << alloc_memory << " " << unit <<std::endl;
		out << "\t" << "Total packets:          " << std::setw(10) << total_packets_ <<std::endl;
		out << "\t" << "Total bytes:        " << std::setw(14) << total_bytes_ <<std::endl;
		if (level > 1) { 
			out << "\t" << "Total validated packets:" << std::setw(10) << total_validated_packets_ <<std::endl;
			out << "\t" << "Total malformed packets:" << std::setw(10) << total_malformed_packets_ <<std::endl;
			if (level > 3) {
			
				out << "\t" << "Total client hellos:    " << std::setw(10) << total_client_hellos_ <<std::endl;
				out << "\t" << "Total server hellos:    " << std::setw(10) << total_server_hellos_ <<std::endl;
				out << "\t" << "Total certificates:     " << std::setw(10) << total_certificates_ <<std::endl;
				out << "\t" << "Total server key exs:   " << std::setw(10) << total_server_key_exchanges_ <<std::endl;
				out << "\t" << "Total certificate reqs: " << std::setw(10) << total_certificate_requests_ <<std::endl;
				out << "\t" << "Total server dones:     " << std::setw(10) << total_server_dones_ <<std::endl;
				out << "\t" << "Total certificates vers:" << std::setw(10) << total_certificate_verifies_ <<std::endl;
				out << "\t" << "Total client key exs:   " << std::setw(10) << total_client_key_exchanges_ <<std::endl;
				out << "\t" << "Total handshakes:       " << std::setw(10) << total_handshake_finishes_ <<std::endl;
				out << "\t" << "Total records:          " << std::setw(10) << total_records_ <<std::endl;
				out << "\t" << "Total allow hosts:      " << std::setw(10) << total_allow_hosts_ <<std::endl;
				out << "\t" << "Total banned hosts:     " << std::setw(10) << total_ban_hosts_ <<std::endl;
			}
			if (level > 2) {
				if (flow_forwarder_.lock())
					flow_forwarder_.lock()->statistics(out);
			}
			if (level > 3) {
				info_cache_->statistics(out);
				host_cache_->statistics(out);
				if (level > 4) {
					showCacheMap(out,host_map_,"SSL Hosts","Host");
				}
			}
		}
	}
}


void SSLProtocol::increaseAllocatedMemory(int value) { 

	info_cache_->create(value);
	host_cache_->create(value);
}

void SSLProtocol::decreaseAllocatedMemory(int value) { 

	info_cache_->destroy(value);
	host_cache_->destroy(value);
}

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(LUA_BINDING)
#if defined(PYTHON_BINDING)
boost::python::dict SSLProtocol::getCounters() const {
	boost::python::dict counters;
#elif defined(RUBY_BINDING)
VALUE SSLProtocol::getCounters() const {
        VALUE counters = rb_hash_new();
#elif defined(LUA_BINDING)
LuaCounters SSLProtocol::getCounters() const {
	LuaCounters counters;
#endif
	addValueToCounter(counters,"packets", total_packets_);
	addValueToCounter(counters,"bytes", total_bytes_);
	addValueToCounter(counters,"allow hosts", total_allow_hosts_);
	addValueToCounter(counters,"banned hosts", total_ban_hosts_);
	addValueToCounter(counters,"client hellos", total_client_hellos_);
	addValueToCounter(counters,"server hellos", total_server_hellos_);
	addValueToCounter(counters,"certificates", total_certificates_);
	addValueToCounter(counters,"server key exchanges", total_server_key_exchanges_);
	addValueToCounter(counters,"certificate requests", total_certificate_requests_);
	addValueToCounter(counters,"server dones", total_server_dones_);
	addValueToCounter(counters,"certificate verifies", total_certificate_verifies_);
	addValueToCounter(counters,"client key exchanges", total_client_key_exchanges_);
	addValueToCounter(counters,"handshake dones", total_handshake_finishes_);
	addValueToCounter(counters,"records", total_records_);

        return counters;
}
#endif

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) 
#if defined(PYTHON_BINDING)
boost::python::dict SSLProtocol::getCache() const {
#elif defined(RUBY_BINDING)
VALUE SSLProtocol::getCache() const {
#endif
        return addMapToHash(host_map_);
}
#endif

} // namespace aiengine
